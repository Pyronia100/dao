import mysql.connector

from datetime import datetime

def open_connection():
    cnx = mysql.connector.connect(host = '172.30.161.150',
                                user='root', password='ppp',
                                database = 'deal_data')
    cursor = cnx.cursor(buffered=True)
    return cursor
  

def query_database_for_prices(direction, instr_name):
    cnx = mysql.connector.connect(host = '172.30.161.150',
                                user='root', password='ppp',
                                database = 'deal_data')
    cursor = cnx.cursor(buffered=True)
    query_string = "SELECT instrument_id FROM instrument WHERE instrument_name=\'{}\'".format(instr_name)
    #print(query_string)
    query = (query_string)
    cursor.execute(query)
    records = cursor.fetchall()
    inst_id = records[0][0]

    query_string = "SELECT price,quantity FROM deal WHERE instrument_id={} AND type=\'{}\'".format(inst_id,direction)
    deal_query = (query_string)
    cursor.execute(deal_query)
    records = cursor.fetchall()
    cursor.close()
    cnx.close()  
    return records
    
def extract_price(records):
    price_list = []
    for price in records:
        price_list.append(price[0])
    return price_list

def query_database_history(start_date,end_date):
    cnx = mysql.connector.connect(host = '172.30.161.150',
                                user='root', password='ppp',
                                database = 'deal_data')
    cursor = cnx.cursor(buffered=True)
    query_string = "SELECT * FROM deal WHERE time BETWEEN \'{}\' and \'{}\'".format(start_date, end_date)

    query = (query_string)
    cursor.execute(query)
    records = cursor.fetchall()
    cursor.close()
    cnx.close()  
    return records;

def query_database_end_positions(start_date,end_date):
    cnx = mysql.connector.connect(host = '172.30.161.150',
                                user='root', password='ppp',
                                database = 'deal_data')
    cursor = cnx.cursor(buffered=True)
    query_string = "SELECT price,type,quantity FROM deal WHERE time BETWEEN \'{}\' and \'{}\'".format(start_date, end_date)

    query = (query_string)
    cursor.execute(query)
    records = cursor.fetchall()
    cursor.close()
    cnx.close()  
    return records; 
