import mysql.connector
import flask
from flask import request, jsonify, Response
import report_queries, report_functions
from sseclient import SSEClient
import json
from data_insertions import insert_data
from datetime import datetime
from flask_cors import CORS, cross_origin

app = flask.Flask(__name__)
app.config["DEBUG"] = True
CORS(app)


@app.route('/', methods=['GET'])
def home():
    return "<h1>Deals archive</h1><p>This site is a prototype API.</p>"


def converttojson(data):
    data2 = []
    listofHeaders = ['deal_id', 'instrument_name', 'cp_name', 'price', 'type', 'quantity', 'time']
    big_json = []
    for tuple in data:
        lst = list(tuple)
        lst[6] = str(tuple[6])
        #print(lst[2])
        lst[2] = str(access_cpty(lst[2]))
        lst[1] = str(access_instr(lst[1]))
        data2.append(lst)
        zipbObj = zip(listofHeaders, lst)
        dictObj = dict(zipbObj)
        big_json.append(dictObj)
    deals = big_json
    return deals


def access_deals():
    cnx = mysql.connector.connect(host='172.30.161.150',
                                  user='root', password='ppp',
                                  database='deal_data')
    cursor = cnx.cursor(buffered=True)
    query = "SELECT deal_id, instrument_id, cp_id, price, type, quantity, time FROM deal"
    cursor.execute(query)
    data = cursor.fetchall()
    cursor.close()
    cnx.close()
    return data

def access_cpty(cpty_id):
    cnx = mysql.connector.connect(host='172.30.161.150',
                                  user='root', password='ppp',
                                  database='deal_data')
    cursor = cnx.cursor(buffered=True)
    query = "SELECT cp_name FROM counter_party WHERE cp_id = \'{}\'".format(cpty_id)
    cursor.execute(query)
    for (cp_name) in cursor:
        return cp_name[0]
    cursor.close()
    cnx.close()
def access_instr(instr_id):
    cnx = mysql.connector.connect(host='172.30.161.150',
                                  user='root', password='ppp',
                                  database='deal_data')
    cursor = cnx.cursor(buffered=True)
    query = "SELECT instrument_name FROM instrument WHERE instrument_id = \'{}\'".format(instr_id)
    cursor.execute(query)
    for (instr_name) in cursor:
      instr_name = instr_name[0]
    cursor.close()
    cnx.close()
    return instr_name


def access_full_instr():
    cnx = mysql.connector.connect(host='172.30.161.150',
                                  user='root', password='ppp',
                                  database='deal_data')
    cursor = cnx.cursor(buffered=True)
    query = "SELECT * FROM instrument"
    cursor.execute(query)
    data = cursor.fetchall()
    cursor.close()
    cnx.close()
    return data

@app.route('/database-status', methods=['GET'])
def check_status():
    try:
        cnx = mysql.connector.connect(host='172.30.161.150',
                                      user='root', password='ppp',
                                      database='deal_data')
    except mysql.connector.errors.InterfaceError:
        connected = [{'databaseStatus':'down'}]
        return jsonify(connected)

    connected = [{'databaseStatus' : 'up'}]
    # cursor.close()
    # cnx.close()
    return jsonify(connected)

@app.route('/deals/all', methods=['GET'])
def get_deals():
    data = access_deals()
    json = converttojson(data)
    return jsonify(json)

# mysql.connector.errors.InterfaceError

@app.route('/deals', methods=['GET'])
def api_id():
    if 'deal_id' in request.args:
        id = int(request.args['deal_id'])
        string = 'deal_id'
    elif 'cp_id' in request.args:
        id = int(request.args['cp_id'])
        string = 'cp_id'
    elif 'instrument_id' in request.args:
        id = int(request.args['instrument_id'])
        string = 'instrument_id'
    elif 'price' in request.args:
        id = int(request.args['price'])
        string = 'price'
    elif 'quantity' in request.args:
        id = int(request.args['quantity'])
        string = 'quantity'
    elif 'time' in request.args:
        id = str(request.args['time'])
        string = 'time'
    elif 'type' in request.args:
        id = str(request.args['type'])
        string = 'type'
    else:
        return "Error: No id field provided. Please specify an id."
    results = []
    data = access_deals()
    json = converttojson(data)
    for deal in json:
        if deal[string] == id:
            results.append(deal)
    return jsonify(results)

@app.route('/average-price', methods=['GET'])
def average_price():
    if 'instrument' in request.args:
        instr = str(request.args['instrument'])
    else:
        return "Error: No id field provided. Please specify an id."
    # if 'type' in request.args:
    #     type = str(request.args['instrument'])
    #     string = type

    buy_price_list = report_queries.query_database_for_average_prices('B', instr)
    if ((len(buy_price_list)) != 0):
        buy_average = report_functions.calculate_avg_buy_and_sell_price(buy_price_list)
        buy_average = buy_average/100
    else:
        buy_average=0
    sell_price_list = report_queries.query_database_for_average_prices('S', instr)
    if ((len(sell_price_list)) != 0):
        sell_average = report_functions.calculate_avg_buy_and_sell_price(sell_price_list)
        sell_average = sell_average/100
    else:
        sell_average=0
    instrument = [{'buy_average' : buy_average, 'sell_average' : sell_average}]
    return jsonify(instrument)

@app.route('/average-prices', methods=['GET'])
def access_average_prices():
    data = access_full_instr()
    list = []
    for tuple in data:
        list.append(tuple[1])

    print (list)
    json = []
    for item in list:
        buy_price_list = report_queries.extract_price(report_queries.query_database_for_prices('B', item))
        if ((len(buy_price_list)) != 0):
            buy_average = report_functions.calculate_avg_buy_and_sell_price(buy_price_list)
            buy_average = buy_average / 100
        else:
            buy_average = 0
        sell_price_list = report_queries.extract_price(report_queries.query_database_for_prices('S', item))
        if ((len(sell_price_list)) != 0):
            sell_average = report_functions.calculate_avg_buy_and_sell_price(sell_price_list)
            sell_average = sell_average / 100
        else:
            sell_average = 0

        json.append(dict({'instrumentName':item,'buyAverage':buy_average,'sellAverage':sell_average}))

    return jsonify(json)

@app.route('/ending-position', methods=['GET'])
def ending_position():
    dates = return_first_and_last_time()
    #print(dates)
    records = report_queries.query_database_end_positions(dates[1],dates[0])
    endposition =report_functions.calculate_end_position(records)
    endposition=endposition/100
    return jsonify([{"endingPosition" : endposition}])

@app.route('/realized-profit', methods=['GET'])
@cross_origin()
def realized_profit():
    data = access_full_instr()
    list = []
    for tuple in data:
        list.append(tuple[1])

    print(list)
    json = []
    for item in list:
        profit =report_functions.profit_calculated(item)
        realized_profit=profit[0]/100
        json.append({"instrumentName":item,"realizedProfit":realized_profit})

    return jsonify(json)

@app.route('/effective-profit', methods=['GET'])
def effective_profit():
    data = access_full_instr()
    list = []
    for tuple in data:
        list.append(tuple[1])

    print(list)
    json = []
    for item in list:
        profit =report_functions.profit_calculated(item)
        effective_profit=profit[1]/100
        json.append({"instrumentName":item,"effectiveProfit":effective_profit})

    return jsonify(json)

def return_first_and_last_time():
    cnx = mysql.connector.connect(host='172.30.161.150',
                                  user='root', password='ppp',
                                  database='deal_data')
    cursor = cnx.cursor(buffered=True)
    query1 = "select max(time) from deal"
    query2 = "select min(time) from deal"
    cursor.execute(query1)
    for (maxdate) in cursor:
      maxdate = maxdate[0]
    cursor.execute(query2)
    for (mindate) in cursor:
      mindate = mindate[0]
    cursor.close()
    cnx.close()
    return[maxdate,mindate]
@app.route('/deals/stream')
def stream():
    # messages = SSEClient('http://localhost:8080/datastream')
    def eventStream():
        messages = SSEClient('http://datagenerator-datagenerator.apps.dbgrads-6eec.openshiftworkshop.com/datastream')

        for msg in messages:

            outputMsg = msg.data
            outputJS = json.loads(outputMsg)
    # FilterName = "data"
            print(outputJS)
            insert_data(outputJS)

            # wait for source data to be available, then push it
            yield 'data:' + str(outputJS).replace("'", '"') + "\n\n"

    return Response(eventStream(), mimetype="text/event-stream")

# @app.route('/login', methods=['GET', 'POST'])
# def login():
#     # user_name = request.args.get("username")
#     # hashed_password = request.args.get("passwordHash")
#     # print (user_name)
#     # print (hashed_password)
#     data = request.get_json()
#     user_name = data['username']
#     password = data['passwordHash']
#     cnx = mysql.connector.connect(host='172.30.161.150',
#                                   user='root', password='ppp',
#                                   database='deal_data')
#     cursor = cnx.cursor(buffered=True)
#     query = "SELECT password, user_role FROM shadow WHERE user_name=\'{}\'".format(user_name)
#     cursor.execute(query)
#     for (data) in cursor:
#         password_hash = int(data[0])
#         user_role = data[1]
    # cursor.close()
    # cnx.close()
#     if password_hash == password:
#         return jsonify([{'username' : user_name, 'role':user_role}])
#     else:
#         return jsonify([{}])

def bootapp():
    app.run(port=8080, debug=True, threaded=True,host=('0.0.0.0'))

# 7y9vJ3PBjncu2pvBEmLv
